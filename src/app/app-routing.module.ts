import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SignUpPageComponent} from './pages/signuppagecomponent/sign-up-page.component';
import {ProductComponent} from './product/product.component';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';
import {UserPersonalInfoComponent} from './pages/user-personal-info/user-personal-info.component';


const routes: Routes = [
  {path: 'signUp', component: SignUpPageComponent},
  {path: 'products', component: ProductComponent},
  {path: '', redirectTo: '/products', pathMatch: 'full'},
  {path: 'not-found', component: PageNotFoundComponent},
  {path: 'userPersonalInfo', component: UserPersonalInfoComponent},
  {path: '**', redirectTo: '/not-found', pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule {
}
